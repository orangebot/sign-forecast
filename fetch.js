﻿//curl https://api.weather.gov/points/39.7456,-97.0892
//curl https://api.weather.gov/gridpoints/TOP/31,80/forecast
//curl http://localhost:55555/points/39.7456,-97.0892
//curl http://localhost:55555/gridpoints/TOP/31,80/forecast

//The latitude and longitude of the area.
//const loc = "LAT,LON";
const loc = "42.4382,-83.1191";
//Number of days to show.
const numdays = 2;
//How often to pull new weather data.
const update = 3600;
//Time to wait before showing the next day's weather.
const cycle = 6;
//The URL of the inital API.
const api = "https://api.weather.gov/points/" + loc;



//Sleep for n seconds.
async function sleep(n) {
	return new Promise(resolve => setTimeout(resolve, n * 1000));
}


//Request a URL and return the data.
async function get(url) {
	return new Promise((resolve, reject) => {
		const req = new XMLHttpRequest();
		req.onload = () => { resolve(req.responseText); }
		req.onerror = () => { throw req.statusText; }
		req.open("GET", url, true); //true for async
		//req.setRequestHeader("Cache-Control", "no-cache");
		req.send();
	});
}


//Function to grab the forcast for the next few days.
async function getForecast(url) {
	//Make a string describing the weather for the next few days.
	console.log("getForecast");
	var dat = await get(url)
		.then((raw) => { return JSON.parse(raw).properties.periods; });
	var desc = [];
	for (var i = 0;
		(i < (numdays * 2) && i < dat.length);
		++i)
	{
		desc[i] = dat[i].name
			+ ": "
			+ dat[i].temperature
			+ ((dat[i].temperatureUnit === "F") ? "℉" : "℃")
			+ ", "
			+ dat[i].shortForecast;
	}
	return desc;
}


//Show the weather on the display.
async function showForecast(forecast, ele) {
	for (var i = 0; i < forecast.length; ++i) {
		//console.log("showing " + i + " " + forecast[i]);
		ele.innerHTML = forecast[i];
		await sleep(cycle);
	}
	
	return 1;
}


async function main() {
	var ele = document.getElementById("forecast");
	var forecast = [];
	var url;
	
	try {
		//Get the url of the forecast data.
		forecast = await get(api)
			.then((d) => { return JSON.parse(d).properties.forecast; })
			.then((u) => { url = u; return getForecast(u); });
		
		//Get forecast data at the specified interval.
		setInterval(() => {
			getForecast(url).then((f) => {forecast = f;})
		}, update * 1000);
		
		while (1) {
			console.log("Showing forecast");
			await showForecast(forecast, ele);
		}
	}
	catch(e) {
		console.error(e);
		ele.innerHTML = e.message;
		await sleep(10);
		location.reload(true);
	}
}


main();

